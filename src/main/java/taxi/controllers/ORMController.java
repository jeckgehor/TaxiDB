package taxi.controllers;

import taxi.service.ORMService;
import taxi.domain.CarsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by new on 08.08.2016.
 */
@Controller
public class ORMController {

    @Autowired
    private ORMService ormService;

    @RequestMapping(value = "/ormFindAllCars", method = RequestMethod.GET)
    public ModelAndView ormFindAllCars() {
        System.out.println("ORMController ormFindAllCars is called");
        List<CarsEntity> cars = ormService.queryFindAllCars();
        System.out.println(cars.size());
        return new ModelAndView("orm", "resultObject", cars);
    }
}
