package taxi.service;

import taxi.domain.CarsEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by new on 08.08.2016.
 */
@Repository
@Transactional //need to update\delete queries. Don't forget <tx:annotation-driven/>
public class ORMService {

    @PersistenceContext
    private EntityManager entityManager;

    public List<CarsEntity> queryFindAllCars() {
        System.out.println("ORMService queryfindAllCars is called");
        String query = "from CarsEntity";
        TypedQuery<CarsEntity> typedQuery = entityManager.createQuery(query, CarsEntity.class);
        return typedQuery.getResultList();
    }
}
