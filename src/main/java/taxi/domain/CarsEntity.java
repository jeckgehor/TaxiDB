package taxi.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by new on 08.08.2016.
 */
@Entity
@Table(name = "cars")
public class CarsEntity {
    @Id
    @Column(name = "state_number")
    private String stateNumber;
    @Basic
    @Column(name = "produñer")
    private String producer;
    @Basic
    @Column(name = "model")
    private String model;
    @Temporal(TemporalType.DATE)
    @Column(name = "manuf_year")
    private Date manufYear;
    @Basic
    @Column(name = "color")
    private String color;
    @Temporal(TemporalType.DATE)
    @Column(name = "date_added")
    private Date dateAdded;

    public CarsEntity() {
    }

    public CarsEntity(String stateNumber, String producer, String model, Date manufYear, String color, Date dateAdded) {
        this.stateNumber = stateNumber;
        this.producer = producer;
        this.model = model;
        this.manufYear = manufYear;
        this.color = color;
        this.dateAdded = dateAdded;
    }


    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }


    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public Date getManufYear() {
        return manufYear;
    }

    public void setManufYear(Date manufYear) {
        this.manufYear = manufYear;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarsEntity that = (CarsEntity) o;

        if (stateNumber != null ? !stateNumber.equals(that.stateNumber) : that.stateNumber != null) return false;
        if (producer != null ? !producer.equals(that.producer) : that.producer != null) return false;
        if (model != null ? !model.equals(that.model) : that.model != null) return false;
        if (manufYear != null ? !manufYear.equals(that.manufYear) : that.manufYear != null) return false;
        if (color != null ? !color.equals(that.color) : that.color != null) return false;
        if (dateAdded != null ? !dateAdded.equals(that.dateAdded) : that.dateAdded != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = stateNumber != null ? stateNumber.hashCode() : 0;
        result = 31 * result + (producer != null ? producer.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (manufYear != null ? manufYear.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (dateAdded != null ? dateAdded.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CarsEntity{" +
                "stateNumber='" + stateNumber + '\'' +
                ", producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", manufYear=" + manufYear +
                ", color='" + color + '\'' +
                ", dateAdded=" + dateAdded +
                '}';
    }
}
