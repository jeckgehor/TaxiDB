package taxi.dao;

import taxi.domain.CarsEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * Created by new on 08.08.2016.
 */
public class CarEntityDAOImpl implements CarEntityDAO {

    private SessionFactory factory;
    private Session session;

    public CarEntityDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public String create(CarsEntity car) {
        if (session == null){
            session = factory.openSession();
        }
        String number = null;
        try {
            session.beginTransaction();
            number = (String)session.save(car);
            session.getTransaction().commit();
            return number;
        }catch (HibernateException e){
            session.getTransaction().rollback();
        } finally {
            if (session!=null)
                session.close();
        }
        return number;
    }

    @Override
    public CarsEntity read(Long id) {
        if (session == null){
            session = factory.openSession();
        }
        try{
            return (CarsEntity)session.get(CarsEntity.class,id);
        }catch (HibernateException e){
            //log.error("Transaction failed");
        }finally {
            if (session!=null)
                session.close();
        }
        return null;
    }

    @Override
    public void update(CarsEntity car) {
        if (session == null){
            session = factory.openSession();
        }
        try {
            session.beginTransaction();
            session.update(car);
            session.getTransaction().commit();
        }catch (HibernateException e){
            // log.error("Transaction failed");
            session.getTransaction().rollback();
        }finally {
            if (session!=null)
                session.close();
        }
    }

    @Override
    public void delete(CarsEntity car) {
        if (session == null){
            session = factory.openSession();
        }
        try {
            session.beginTransaction();
            session.delete(car);
            session.getTransaction().commit();
        }catch (HibernateException e){
            // log.error("Transaction failed");
            session.getTransaction().rollback();
        }finally {
            if (session!=null)
                session.close();
        }
    }
}
