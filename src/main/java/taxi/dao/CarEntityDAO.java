package taxi.dao;

import taxi.domain.CarsEntity;

/**
 * Created by new on 08.08.2016.
 */
public interface CarEntityDAO {
    String create(CarsEntity car);
    CarsEntity read(Long id);
    void update(CarsEntity car);
    void delete(CarsEntity car);
}
